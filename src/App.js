import React, {Component} from "react";
import "./App.css";
import Square from "./components/element/Square";
import ResetButton from "./components/reset/ResetButton";
import Counter from "./components/Counter/Counter";


class App extends Component {
    state = {
        squareClasses: [],
        count: 0,
        gameContinue: true,
        gameAnswer: 0
    };

    resetGame = () => {
        let randomSquare = Math.floor(Math.random() * (36 - 1) + 1);
        let array = [];
        for (let i = 0; i < 36; i++) {
            let name;
            if (randomSquare === i)
                name = "mainSquare square answerClass";
            else
                name = "mainSquare square";
            array.push({name: name, id: i})
        }
        this.setState({squareClasses: array, count: 0, gameAnswer: randomSquare, gameContinue: true});

    };
    removeSquare = (id) => {
        if (!this.state.gameContinue) {
            alert("Вы уже нашли кольцо!!! Начните новую игру");
            return;
        }
        const index = this.state.squareClasses.findIndex(p => p.id === id);
        let gameContinue = true;
        if (index === this.state.gameAnswer) {
            gameContinue = false;
            alert("Кольцо найдено")
        }
        const square = [...this.state.squareClasses];
        let counter = this.state.count;
        counter++;
        const c = square[index].name.replace("mainSquare", "hiddenSquare");
        c.replace("mainSquare", "hiddenSquare");
        square[index].name = c;
        this.setState({squareClasses: square, count: counter, gameContinue: gameContinue});


    };


    render() {


        return (

            <div className="App">
                <ResetButton reset={this.resetGame}>Start New Game</ResetButton>
                <div className="container"> {

                    this.state.squareClasses.map(square => {

                        return (
                            <Square squareClass={square.name}
                                    remove={() => this.removeSquare(square.id)}
                                    key={square.id}></Square>
                        );

                    })
                }</div>
                <Counter count={this.state.count}></Counter>
            </div>)
    }
}

export default App;
