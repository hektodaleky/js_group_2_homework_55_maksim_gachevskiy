/**
 * Created by Max on 24.01.2018.
 */
import React from "react";
import './ResetButton.css'
const ResetButton = props => {
    return (<button className="reset-button" onClick={props.reset}>
        {props.children}
    </button>);
};
export default ResetButton;