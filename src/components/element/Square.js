/**
 * Created by Max on 24.01.2018.
 */
import React from 'react';
import './Square.css';

const Square=props=>{
    return(
        <div className={props.squareClass} onClick={props.remove}></div>
    )
};
export default Square;
